import time
import streamlit as st
from kravicky.core import pnl_model
from kravicky.core import types
import pandas as pd

"""Main module for the streamlit app"""
import logging

import streamlit as st
from kravicky.dashboard import pages


def main() -> None:
    """Main function of the App"""
    # st.set_page_config(page_title="Kravicky dashboard", layout="wide")
    # st.sidebar.image(
    #     "data/images/credits.jpg", use_column_width=True,
    # )
    st.sidebar.title("Kravicky - dashboard")

    st.sidebar.header("Navigation")
    all_pages = pages.get_all()
    page = st.sidebar.radio("Go to", all_pages, format_func=lambda page: page.NAME)

    with st.spinner(f"Loading {page.NAME} ..."):
        logging.info(f"Loading {page.NAME}: {page}")
        st.title(page.NAME)
        page.write()  # type: ignore

    # st.sidebar.header("Inputs")
    # hpfcs = ", ".join(state.get_state().hpfcs.columns)
    # st.sidebar.markdown(f"Loaded HPFCs: {hpfcs}")
    # st.sidebar.markdown(f"Country: {state.get_state().country}")


if __name__ == "__main__":
    main()
