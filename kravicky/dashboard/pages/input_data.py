from typing import Optional
from pandas.core.accessor import register_index_accessor
from kravicky.dashboard.pages.base import PageABC
import streamlit as st
import time
import pandas as pd
from streamlit.uploaded_file_manager import UploadedFile
from kravicky.dashboard import state
import logging


@st.cache(show_spinner=False, allow_output_mutation=False)
def parse_dataframes(input_data: Optional[UploadedFile], index: str) -> pd.DataFrame:
    if not input_data:
        logging.info(f"No data")
        return pd.DataFrame()
    logging.info(f"Data found")
    data = pd.read_excel(input_data, index_col=index).fillna(0)
    return data


class Input(PageABC):
    NAME = "Input data"

    def write(self) -> None:
        clear_inputs = st.button("Clear all inputs")
        if clear_inputs:
            state.clear_state()
            st.info("Application state cleared.")

        st.header("General parameters")
        col1, col2 = st.beta_columns(2)
        with col1:
            if self._app_state.pork_weight:
                value = self._app_state.pork_weight
            else:
                value = 95
            pork_weight = st.slider("Pork weight", min_value=50, max_value=150, value=value, step=5)
            self._app_state.pork_weight = pork_weight
        with col2:
            if self._app_state.beef_weight:
                value = self._app_state.beef_weight
            else:
                value = 350
            beef_weight = st.slider("Beef weight", min_value=250, max_value=450, value=value, step=10)
            self._app_state.beef_weight = beef_weight

        col1, col2, col3 = st.beta_columns(3)
        with col1:
            if self._app_state.number_of_pork:
                value = self._app_state.number_of_pork
            else:
                value = 500
            number_of_pork = st.number_input(
                "Number of pork available", min_value=0, max_value=1000, value=value
            )
            self._app_state.number_of_pork = number_of_pork
        with col2:
            if self._app_state.number_of_beef_front:
                value = self._app_state.number_of_beef_front
            else:
                value = 100
            number_of_beef_front = st.number_input(
                "Number of beef fronts available", min_value=0, max_value=200, value=value
            )
            self._app_state.number_of_beef_front = number_of_beef_front
        with col3:
            if self._app_state.number_of_beef_back:
                value = self._app_state.number_of_beef_back
            else:
                value = 100
            number_of_beef_back = st.number_input(
                "Number of beef backs available", min_value=0, max_value=200, value=value
            )
            self._app_state.number_of_beef_back = number_of_beef_back

        if self._app_state.waste_disposal_costs:
            value = self._app_state.waste_disposal_costs
        else:
            value = 7.0
        self._app_state.waste_disposal_costs = st.number_input(
            label="Waste disposal costs Kč/kg.", value=value, step=0.1
        )
        st.header("File inputs")
        col1, col2 = st.beta_columns(2)
        with col1:
            definition_data = st.file_uploader(
                "Products vs components - definition", type=["xlsx", "xls"], accept_multiple_files=False,
            )
        with col2:
            volumes = st.file_uploader("Volumes", type=["xlsx", "xls"], accept_multiple_files=False)
        with col1:
            parameters = st.file_uploader("Parameters", type=["xlsx", "xls"], accept_multiple_files=False)
        with col2:
            price_products = st.file_uploader(
                "Price - products", type=["xlsx", "xls"], accept_multiple_files=False
            )

        with col1:
            volume_limit = st.file_uploader(
                "Product availability", type=["xlsx", "xls"], accept_multiple_files=False
            )

        process_files = st.button("Load files")
        if process_files:
            with st.spinner(text="Loading data!"):
                if definition_data:
                    self._app_state.definition = parse_dataframes(
                        definition_data, index="Produkty / Komponenty"
                    )
                if volumes:
                    self._app_state.volumes = parse_dataframes(volumes, index="product")
                if parameters:
                    self._app_state.parameters = parse_dataframes(parameters, index="component")
                if volume_limit:
                    self._app_state.volume_limit = parse_dataframes(volume_limit, index="product")
                if price_products:
                    self._app_state.price_products = parse_dataframes(price_products, index="code")
            st.info("Data loaded.")

