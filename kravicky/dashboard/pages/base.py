import abc

from kravicky.dashboard import state


class PageABC(abc.ABC):
    """Page abstract base class

    Page classes must define NAME class attribute and write method
    and can override the default REPORT value to be excluded from the report pages.
    """

    @property  # type: ignore
    @staticmethod
    @abc.abstractmethod
    def NAME() -> str:  # noqa: N802
        # we can only define abstract attribute as an abstract property
        pass

    def __init__(self) -> None:
        self._app_state = state.get_state()

    @abc.abstractmethod
    def write(self) -> None:  # noqa: U100
        pass
