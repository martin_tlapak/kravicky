from logging import warning
from kravicky.dashboard.pages.base import PageABC
import streamlit as st
import time
from kravicky.dashboard import state
from kravicky.core.pnl_model import PnLModel
from kravicky.core.off_the_top_of_my_head_model import OffTheTopOfMyHeadModel
from kravicky.core.benchmark_model import BenchMarkModel
from kravicky.core.types import PnLModelInput, BenchMarkModelInput, OffTheTopOfMyHeadInput


class Compute(PageABC):
    NAME = "Optimal solution"
    MODEL_TYPE = ["BENCHMARK MODEL", "PnL MODEL", "OFF THE TOP OF MY HEAD"]

    def state_input(self) -> None:
        self._app_state = state.get_state()

        model_type = st.selectbox("Model type", self.MODEL_TYPE)
        if model_type == "PnL MODEL":
            include_availability_limits = st.checkbox(label="Include availability limits.", value=True)
        elif model_type == "OFF THE TOP OF MY HEAD":
            st.header("I WOULDN’T BE SURPRISED IF…")
            col1, col2, col3 = st.beta_columns(3)
            with col1:
                pork_estimate = st.number_input(
                    "Pork estimate", value=self._app_state.number_of_pork,max_value=self._app_state.number_of_pork
                )
            with col2:
                beef_front_estimate = st.number_input(
                    "Beef front estimate",
                    value=self._app_state.number_of_beef_front,
                    max_value=self._app_state.number_of_beef_front,
                )
            with col3:
                beef_back_estimate = st.number_input(
                    "Beef back estimate",
                    value=self._app_state.number_of_beef_back,
                    max_value=self._app_state.number_of_beef_back,
                )
            include_availability_limits = st.checkbox(label="Include availability limits.", value=False)
        run = st.button("Run",)
        with st.beta_expander("Show inputs"):
            st.header("General parameters")
            st.subheader("Weights")
            col1, col2 = st.beta_columns(2)
            with col1:
                if self._app_state.pork_weight:
                    st.write(f"Pork weight is set to {self._app_state.pork_weight} kg.")
                else:
                    st.warning("Pork weight is not set. Set them in Input data page!")
            with col2:
                if self._app_state.beef_weight:
                    st.write(f"Beef weight is set to {self._app_state.beef_weight} kg.")
                else:
                    st.warning("Beef weight is not set. Set them in Input data page!")
            st.subheader("Number of available parts")
            col1, col2, col3 = st.beta_columns(3)
            with col1:
                if self._app_state.number_of_pork:
                    st.write(f"Number of pork complets is set to {self._app_state.number_of_pork}.")
                else:
                    st.warning("Number of pork complets is not set. Set them in Input data page!")
            with col2:
                if self._app_state.number_of_beef_front:
                    st.write(f"Number of beef fronts is set to {self._app_state.number_of_beef_front}.")
                else:
                    st.warning("Number of pork complets is not set. Set them in Input data page!")
            with col3:
                if self._app_state.number_of_beef_back:
                    st.write(f"Number of beef backs is set to {self._app_state.number_of_beef_back}.")
                else:
                    st.warning("Number of pork complets is not set. Set them in Input data page!")

            st.header("Volumes and availability")
            st.subheader("Volumes")
            if not self._app_state.volumes.empty:
                st.write(self._app_state.volumes)
            else:
                st.warning("Volumes data are not loaded.  Input them on Input data page!")
            st.subheader("Availability")
            if not self._app_state.volume_limit.empty:
                st.write(self._app_state.volume_limit)
            else:
                st.warning("Availability data are not loaded.  Input them on Input data page!")

            st.header("Parameters")
            st.subheader("Parameters")
            if not self._app_state.parameters.empty:
                st.write(self._app_state.parameters)
            else:
                st.warning("Parameters data are not loaded.  Input them on Input data page!")

            st.header("Price data")
            st.subheader("Products prices")
            if not self._app_state.price_products.empty:
                st.write(self._app_state.price_products)
            else:
                st.warning("Price products data are not loaded.  Input them on Input data page!")

        if run:
            if model_type == "OFF THE TOP OF MY HEAD":
                with st.spinner(text="It takes some time!"):
                    if self._app_state.definition.empty:
                        st.error("Please provide definition data. Input them on Input data page!")
                        return
                    if self._app_state.volumes.empty:
                        st.error("Please provide volumes data. Input them on Input data page!")
                        return
                    if self._app_state.parameters.empty:
                        st.error("Please provide parameters data. Input them on Input data page!")
                        return
                    if self._app_state.price_products.empty:
                        st.error("Please provide price products data. Input them on Input data page!")
                        return
                    input_data = OffTheTopOfMyHeadInput(
                        weight_beef=self._app_state.beef_weight,
                        weight_pork=self._app_state.pork_weight,
                        number_of_pork_complet=self._app_state.number_of_pork,
                        number_of_beef_front=self._app_state.number_of_beef_front,
                        number_of_beef_back=self._app_state.number_of_beef_back,
                        definition=self._app_state.definition,
                        volumes=self._app_state.volumes,
                        parameters=self._app_state.parameters,
                        price_products=self._app_state.price_products,
                        volume_limit=self._app_state.volume_limit,
                        include_availability_limits=include_availability_limits,
                        waste_disposal_costs=self._app_state.waste_disposal_costs,
                        pork_estimate=pork_estimate,
                        beef_front_estimate=beef_front_estimate,
                        beef_back_estimate=beef_back_estimate,
                    )
                    model = OffTheTopOfMyHeadModel(input_parameters=input_data)
                    model.compute()
                    if model.model.num_solutions:
                        self._app_state.model_ottomh = model
                        st.write(model.prepare_number_of_parts())
                        st.area_chart(model.prepare_number_of_parts())
                        st.balloons()
                    else:
                        st.warning("Model cannot find feasible solution. Please adjust input parameters.")
            if model_type == "PnL MODEL":
                with st.spinner(text="It takes some time!"):
                    if self._app_state.definition.empty:
                        st.error("Please provide definition data. Input them on Input data page!")
                        return
                    if self._app_state.volumes.empty:
                        st.error("Please provide volumes data. Input them on Input data page!")
                        return
                    if self._app_state.parameters.empty:
                        st.error("Please provide parameters data. Input them on Input data page!")
                        return
                    if self._app_state.price_products.empty:
                        st.error("Please provide price products data. Input them on Input data page!")
                        return
                    input_data = PnLModelInput(
                        weight_beef=self._app_state.beef_weight,
                        weight_pork=self._app_state.pork_weight,
                        number_of_pork_complet=self._app_state.number_of_pork,
                        number_of_beef_front=self._app_state.number_of_beef_front,
                        number_of_beef_back=self._app_state.number_of_beef_back,
                        definition=self._app_state.definition,
                        volumes=self._app_state.volumes,
                        parameters=self._app_state.parameters,
                        price_products=self._app_state.price_products,
                        volume_limit=self._app_state.volume_limit,
                        include_availability_limits=include_availability_limits,
                        waste_disposal_costs=self._app_state.waste_disposal_costs,
                    )
                    model = PnLModel(input_parameters=input_data)
                    model.compute()
                    if model.model.num_solutions:
                        self._app_state.model_pnl = model
                        st.write(model.prepare_number_of_parts())
                        st.area_chart(model.prepare_number_of_parts())
                        st.balloons()
                    else:
                        st.warning("Model cannot find feasible solution. Please adjust input parameters.")

            elif model_type == "BENCHMARK MODEL":
                with st.spinner(text="It takes some time!"):
                    if self._app_state.definition.empty:
                        st.error("Please provide definition data. Input them on Input data page!")
                        return
                    if self._app_state.volumes.empty:
                        st.error("Please provide volumes data. Input them on Input data page!")
                        return
                    if self._app_state.parameters.empty:
                        st.error("Please provide parameters data. Input them on Input data page!")
                        return

                    input_data = BenchMarkModelInput(
                        weight_beef=self._app_state.beef_weight,
                        weight_pork=self._app_state.pork_weight,
                        number_of_pork_complet=self._app_state.number_of_pork,
                        number_of_beef_front=self._app_state.number_of_beef_front,
                        number_of_beef_back=self._app_state.number_of_beef_back,
                        definition=self._app_state.definition,
                        volumes=self._app_state.volumes,
                        parameters=self._app_state.parameters,
                    )
                    model = BenchMarkModel(input_parameters=input_data)
                    model.compute()
                    if model.model.num_solutions:
                        self._app_state.model_benchmark = model
                        st.write(model.prepare_number_of_parts())
                        st.area_chart(model.prepare_number_of_parts())
                        st.balloons()
                    else:
                        st.warning("Model cannot find feasible solution. Please adjust input parameters.")

    def write(self) -> None:
        self.state_input()
