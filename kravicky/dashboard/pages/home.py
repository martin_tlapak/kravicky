from kravicky.dashboard.pages.base import PageABC
import streamlit as st


class Home(PageABC):
    NAME = "Home"

    def write(self) -> None:  # noqa: U100
        """Used to write the page in the app.py file"""
        with st.spinner("Loading Home ..."):
            st.write(
                """
                This is a Kravicky dashboard.

                Credit to Tomas Blazek!
                """
            )
            st.image("data/images/credits.jpg", width=800)
