from kravicky.dashboard import state
from kravicky.dashboard.pages.base import PageABC
import streamlit as st
import base64
import pandas as pd


class ResultAnalysis(PageABC):
    NAME = "Result analysis"

    def state_inputs(self) -> None:
        self._app_state = state.get_state()
        computed_models = []
        if self._app_state.model_benchmark:
            computed_models.append("BENCHMARK MODEL")
        if self._app_state.model_pnl:
            computed_models.append("PnL MODEL")
        if self._app_state.model_ottomh:
            computed_models.append("OFF THE TOP OF MY HEAD")
        selected_model = st.multiselect(
            label="Models for analysis", options=computed_models, default=computed_models
        )

        if st.button("Prepare analysis!"):
            with st.beta_expander(label="Beef/ pork quantity"):
                st.header("Beef/ pork quantity")
                for model in selected_model:
                    st.subheader(model)
                    if model == "BENCHMARK MODEL":
                        data_numbers_benchmark = self._app_state.model_benchmark.prepare_number_of_parts()
                        st.write(data_numbers_benchmark)
                        st.bar_chart(data_numbers_benchmark)
                    if model == "PnL MODEL":
                        data_numbers_pnl = self._app_state.model_pnl.prepare_number_of_parts()
                        st.write(data_numbers_pnl)
                        st.bar_chart(data_numbers_pnl)
                    if model == "OFF THE TOP OF MY HEAD":
                        data_numbers_ottomh = self._app_state.model_ottomh.prepare_number_of_parts()
                        st.write(data_numbers_ottomh)
                        st.bar_chart(data_numbers_ottomh)

            with st.beta_expander(label="Product availability"):
                st.header("Product availability")
                for model in selected_model:
                    st.subheader(model)
                    if model == "BENCHMARK MODEL":
                        data_availability_benchmark = self._app_state.model_benchmark.product_availability()
                        st.write(data_availability_benchmark)
                        st.line_chart(data_availability_benchmark)
                        st.bar_chart(data_availability_benchmark)
                    if model == "PnL MODEL":
                        data_availability_pnl = self._app_state.model_pnl.product_availability()
                        st.write(data_availability_pnl)
                        st.line_chart(data_availability_pnl)
                        st.bar_chart(data_availability_pnl)
                    if model == "OFF THE TOP OF MY HEAD":
                        data_availability_ottomh = self._app_state.model_ottomh.product_availability()
                        st.write(data_availability_ottomh)
                        st.line_chart(data_availability_ottomh)
                        st.bar_chart(data_availability_ottomh)

            # with st.beta_expander(label="Leftovers analysis"):
            #     st.header("Leftovers analysis")
            #     for model in selected_model:
            #         st.subheader(model)
            #         if model == "BENCHMARK MODEL":
            #             st.subheader("Pork leftovers")
            #             col1, col2 = st.beta_columns((1, 3))
            #             data_pork = self._app_state.model_benchmark.pork_leftovers()
            #             with col1:
            #                 st.write(data_pork)
            #             with col2:
            #                 st.bar_chart(data_pork)
            #             st.subheader("Beef front leftovers")
            #             col1, col2 = st.beta_columns((1, 3))
            #             data_beef_front = self._app_state.model_benchmark.beef_front_leftovers()
            #             with col1:
            #                 st.write(data_beef_front)
            #             with col2:
            #                 st.bar_chart(data_beef_front)
            #             st.subheader("Beef back leftovers")
            #             col1, col2 = st.beta_columns((1, 3))
            #             data_beef_back = self._app_state.model_benchmark.beef_back_leftovers()
            #             with col1:
            #                 st.write(data_beef_back)
            #             with col2:
            #                 st.bar_chart(data_beef_back)
            #         if model == "PnL MODEL":
            #             st.subheader("Pork leftovers")
            #             col1, col2 = st.beta_columns((1, 3))
            #             data_pork = self._app_state.model_pnl.pork_leftovers()
            #             with col1:
            #                 st.write(data_pork)
            #             with col2:
            #                 st.bar_chart(data_pork)
            #             st.subheader("Beef front leftovers")
            #             col1, col2 = st.beta_columns((1, 3))
            #             data_beef_front = self._app_state.model_pnl.beef_front_leftovers()
            #             with col1:
            #                 st.write(data_beef_front)
            #             with col2:
            #                 st.bar_chart(data_beef_front)
            #             st.subheader("Beef back leftovers")
            #             col1, col2 = st.beta_columns((1, 3))
            #             data_beef_back = self._app_state.model_pnl.beef_back_leftovers()
            #             with col1:
            #                 st.write(data_beef_back)
            #             with col2:
            #                 st.bar_chart(data_beef_back)
            with st.beta_expander(label="PnL result"):
                st.header("PnL result")
                for model in selected_model:
                    st.subheader(model)
                    if model == "BENCHMARK MODEL":
                        st.info("PnL result is not implemented for benchmark model!")
                    if model == "PnL MODEL":
                        st.write(
                            f"Expected profit is {-self._app_state.model_pnl.model.objective_value} CZK."
                        )
                    if model == "OFF THE TOP OF MY HEAD":
                        st.write(
                            f"Expected profit is {-self._app_state.model_ottomh.model.objective_value} CZK."
                        )
            with st.beta_expander(label="Download files"):
                st.header("Download files")
                st.write("To download in csv please right-click and save as <some_name>.csv")
                for model in selected_model:
                    st.subheader(model)

                    if model == "BENCHMARK MODEL":
                        col1, col2 = st.beta_columns(2)
                        with col1:
                            st.subheader("Quantity")
                            st.markdown(
                                self.get_table_download_link(data_numbers_benchmark), unsafe_allow_html=True
                            )
                        with col2:
                            st.subheader("Availability")
                            st.markdown(
                                self.get_table_download_link(data_availability_benchmark),
                                unsafe_allow_html=True,
                            )
                        st.header("How to cut")
                        col1, col2, col3 = st.beta_columns(3)
                        with col1:
                            st.subheader("Pork")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_benchmark.get_how_to_cut_pork()
                                ),
                                unsafe_allow_html=True,
                            )
                        with col2:
                            st.subheader("Beef front")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_benchmark.get_how_to_beef_front()
                                ),
                                unsafe_allow_html=True,
                            )
                        with col3:
                            st.subheader("Beef back")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_benchmark.get_how_to_cut_beef_back()
                                ),
                                unsafe_allow_html=True,
                            )
                    if model == "PnL MODEL":
                        col1, col2 = st.beta_columns(2)
                        with col1:
                            st.subheader("Quantity")
                            st.markdown(
                                self.get_table_download_link(data_numbers_pnl), unsafe_allow_html=True
                            )
                        with col2:
                            st.subheader("Availability")
                            st.markdown(
                                self.get_table_download_link(data_availability_pnl), unsafe_allow_html=True
                            )
                        st.header("How to cut")
                        col1, col2, col3 = st.beta_columns(3)
                        with col1:
                            st.subheader("Pork")
                            st.markdown(
                                self.get_table_download_link(self._app_state.model_pnl.get_how_to_cut_pork()),
                                unsafe_allow_html=True,
                            )
                        with col2:
                            st.subheader("Beef front")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_pnl.get_how_to_beef_front()
                                ),
                                unsafe_allow_html=True,
                            )
                        with col3:
                            st.subheader("Beef back")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_pnl.get_how_to_cut_beef_back()
                                ),
                                unsafe_allow_html=True,
                            )
                    if model == "OFF THE TOP OF MY HEAD":
                        col1, col2 = st.beta_columns(2)
                        with col1:
                            st.subheader("Quantity")
                            st.markdown(
                                self.get_table_download_link(data_numbers_ottomh), unsafe_allow_html=True
                            )
                        with col2:
                            st.subheader("Availability")
                            st.markdown(
                                self.get_table_download_link(data_availability_ottomh), unsafe_allow_html=True
                            )
                        st.header("How to cut")
                        col1, col2, col3 = st.beta_columns(3)
                        with col1:
                            st.subheader("Pork")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_ottomh.get_how_to_cut_pork()
                                ),
                                unsafe_allow_html=True,
                            )
                        with col2:
                            st.subheader("Beef front")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_ottomh.get_how_to_beef_front()
                                ),
                                unsafe_allow_html=True,
                            )
                        with col3:
                            st.subheader("Beef back")
                            st.markdown(
                                self.get_table_download_link(
                                    self._app_state.model_ottomh.get_how_to_cut_beef_back()
                                ),
                                unsafe_allow_html=True,
                            )

    def write(self) -> None:
        self.state_inputs()

    @staticmethod
    def get_table_download_link(df: pd.DataFrame) -> str:
        """Generates a link allowing the data in a given panda dataframe to be downloaded
        in:  dataframe
        out: href string
        """
        csv = df.to_csv(index=True)
        b64 = base64.b64encode(csv.encode()).decode()  # some strings <-> bytes conversions necessary here
        return f'<a href="data:file/csv;base64,{b64}">Download csv file</a>'
