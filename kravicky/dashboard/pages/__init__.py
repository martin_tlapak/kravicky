from typing import List, Type
from kravicky.dashboard.pages.base import PageABC
from kravicky.dashboard.pages.compute import Compute
from kravicky.dashboard.pages.home import Home
from kravicky.dashboard.pages.input_data import Input
from kravicky.dashboard.pages.result_analysis import ResultAnalysis

ALL: List[Type[PageABC]] = [Home, Input, Compute, ResultAnalysis]


def get_all() -> List[PageABC]:
    """Return all page instances
    """
    # note: we can inject state here so that page classes do not depend on the state storage directly
    return [page_cls() for page_cls in ALL]
