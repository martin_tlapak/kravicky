from typing import Optional
import pandas as pd
from dataclasses import dataclass, field
import streamlit as st
import streamlit.caching
import kravicky.dashboard.st_state_patch
from kravicky.core.pnl_model import PnLModel
from kravicky.core.benchmark_model import BenchMarkModel
from kravicky.core.off_the_top_of_my_head_model import OffTheTopOfMyHeadModel

_APP_STATE_KEY = "app_state"


@dataclass
class AppState:
    """Holds all the app session state variables
    """

    parameters: pd.DataFrame = field(default_factory=pd.DataFrame)
    volumes: pd.DataFrame = field(default_factory=pd.DataFrame)
    definition: pd.DataFrame = field(default_factory=pd.DataFrame)
    mapping_products: pd.DataFrame = field(default_factory=pd.DataFrame)
    mapping_components: pd.DataFrame = field(default_factory=pd.DataFrame)
    price_products: pd.DataFrame = field(default_factory=pd.DataFrame)
    model_pnl: Optional[PnLModel] = None
    model_benchmark: Optional[BenchMarkModel] = None
    model_ottomh: Optional[OffTheTopOfMyHeadModel] = None
    pork_weight: int = field(default_factory=int)
    beef_weight: int = field(default_factory=int)
    number_of_pork: int = field(default_factory=int)
    number_of_beef_front: int = field(default_factory=int)
    number_of_beef_back: int = field(default_factory=int)
    volume_limit: pd.DataFrame = field(default_factory=pd.DataFrame)
    waste_disposal_costs: float = field(default_factory=float)

    def clear(self) -> None:
        self.parameters = pd.DataFrame()
        self.volumes = pd.DataFrame()
        self.definition = pd.DataFrame()
        self.mapping_components = pd.DataFrame()
        self.mapping_products = pd.DataFrame()
        self.price_products = pd.DataFrame()
        self.model_pnl = None
        self.model_benchmark = None
        self.pork_weight = int()
        self.beef_weight = int()
        self.number_of_pork = int()
        self.number_of_beef_front = int()
        self.number_of_beef_back = int()
        self.volume_limit = pd.DataFrame()
        self.waste_disposal_costs = float()
        self.model_ottomh = None


def get_state() -> AppState:
    """Get the app session state
    """
    session_state = st.State(key=_APP_STATE_KEY)
    if not hasattr(session_state, "app_state"):
        session_state.app_state = AppState()
    return session_state.app_state


def clear_state(clear_cache: bool = True) -> None:
    """Clear the app session state
    """
    session_state = st.State(key=_APP_STATE_KEY)
    session_state.app_state.clear()
    if clear_cache:
        streamlit.caching.clear_cache()
