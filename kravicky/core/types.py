from pydantic import BaseModel
import pandas as pd

from typing import TypeVar

PandasDataFrame = TypeVar("pandas.core.frame.DataFrame")


class PnLModelInput(BaseModel):
    weight_beef: int = 350
    weight_pork: int = 95
    number_of_pork_complet: int
    number_of_beef_front: int
    number_of_beef_back: int
    definition: PandasDataFrame
    volumes: PandasDataFrame
    parameters: PandasDataFrame
    price_products: PandasDataFrame
    volume_limit: PandasDataFrame
    include_availability_limits: bool
    waste_disposal_costs: float


class BenchMarkModelInput(BaseModel):
    weight_beef: int = 350
    weight_pork: int = 95
    number_of_pork_complet: int
    number_of_beef_front: int
    number_of_beef_back: int
    definition: PandasDataFrame
    volumes: PandasDataFrame
    parameters: PandasDataFrame


class OffTheTopOfMyHeadInput(BaseModel):
    weight_beef: int = 350
    weight_pork: int = 95
    number_of_pork_complet: int
    number_of_beef_front: int
    number_of_beef_back: int
    definition: PandasDataFrame
    volumes: PandasDataFrame
    parameters: PandasDataFrame
    price_products: PandasDataFrame
    volume_limit: PandasDataFrame
    include_availability_limits: bool
    waste_disposal_costs: float
    pork_estimate: int
    beef_front_estimate: int
    beef_back_estimate: int

