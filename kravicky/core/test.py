# %%
from kravicky.core import pnl_model
from kravicky.core import types
import pandas as pd

# %%
root_path = "/mnt/c/projekty/privat/kravicky/data/"
parameters = pd.read_excel(root_path + "parameters.xlsx", index_col="component")
volumes = pd.read_excel(root_path + "volume.xlsx", index_col="product")
definition = pd.read_excel(root_path + "definition.xlsx", index_col="Produkty / Komponenty").fillna(0)
mapping_products = pd.read_excel(root_path + "mapping_products.xlsx", index_col="code ")
mapping_components = pd.read_excel(root_path + "mapping_components.xlsx", index_col="code")
price_products = pd.read_excel(root_path + "price_products.xlsx", index_col="code ")
price_components = pd.read_excel(root_path + "price_components.xlsx", index_col="code")
# %%
input_parameters = types.PnLModelInput(
    weight_beef=350,
    weight_pork=95,
    number_of_pork_complet=500,
    number_of_beef_front=100,
    number_of_beef_back=100,
    definition=definition,
    volumes=volumes,
    parameters=parameters,
    price_products=price_products,
    price_components=price_components,
)

# %%
model = pnl_model.PnLModel(input_parameters=input_parameters)
# %%
model.compute()
# %%
model.y[0].x

# %%
model.prepare_number_of_parts()
