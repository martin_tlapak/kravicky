from kravicky.core import types
from mip import Model, xsum, minimize, BINARY
from itertools import product
import pandas as pd


class OffTheTopOfMyHeadModel:
    def __init__(self, input_parameters: types.OffTheTopOfMyHeadInput) -> None:
        self.input_data = input_parameters

    def compute(self):
        definition_matrix = self.input_data.definition.values
        needed_kgs = self.input_data.volumes.T.values[0]
        weight_beef = self.input_data.weight_beef
        weight_pork = self.input_data.weight_pork
        parameters = self.input_data.parameters
        price_products = self.input_data.price_products
        number_of_pork_complet = self.input_data.number_of_pork_complet
        number_of_beef_front = self.input_data.number_of_beef_front
        number_of_beef_back = self.input_data.number_of_beef_back
        volume_limit = self.input_data.volume_limit
        include_limits = self.input_data.include_availability_limits
        waste_disposal_costs = self.input_data.waste_disposal_costs
        pork_estimate = self.input_data.pork_estimate
        beef_front_estimate = self.input_data.beef_front_estimate
        beef_back_estimate = self.input_data.beef_back_estimate

        Q = set(range(len(needed_kgs)))
        H = set(range(number_of_beef_front + number_of_beef_back + number_of_pork_complet))
        HBF = set(range(number_of_beef_front))
        HBB = set(range(number_of_beef_front, number_of_beef_front + number_of_beef_back))
        HPC = set(
            range(
                number_of_beef_front + number_of_beef_back,
                number_of_beef_front + number_of_beef_back + number_of_pork_complet,
            )
        )
        P = set(range(len(parameters)))
        part_in_product = []
        for p in P:
            is_in_product = False
            for q in Q:
                if definition_matrix[q][p] == 1:
                    is_in_product = True
                    continue
            if is_in_product:
                part_in_product.append(1)
            else:
                part_in_product.append(0)
        PBF = [item for item in P if parameters["section"].iloc[item] == "P"]
        PBB = [item for item in P if parameters["section"].iloc[item] == "Z"]
        PPC = [item for item in P if parameters["section"].iloc[item] == "C"]
        self.PBF = PBF
        self.PPC = PPC
        self.PBB = PBB
        self.HPC = HPC
        self.HBF = HBF
        self.HBB = HBB
        self.Q = Q

        model = Model()

        # continuous variable x_{hpq}
        x = [[[model.add_var() for q in Q] for p in P] for h in H]

        # binary variable y_{h}
        y = [model.add_var(var_type=BINARY) for h in H]

        # continous variable z_{q}
        z_p = [model.add_var() for q in Q]
        z_n = [model.add_var() for q in Q]

        model.objective = minimize(
            xsum(-price_products["price"].iloc[q] * (needed_kgs[q] - z_n[q]) for q in Q)
            + xsum(price_products["price"].iloc[q] * z_p[q] for q in Q)
            + xsum(waste_disposal_costs * z_p[q] for q in Q)
        )
        counter = 0
        for h in HPC:
            if counter < pork_estimate:
                model += y[h] == 1
                counter += 1
            else:
                model += y[h] == 0
        counter = 0
        for h in HBF:
            if counter < beef_front_estimate:
                model += y[h] == 1
                counter += 1
            else:
                model += y[h] == 0

        counter = 0
        for h in HBB:
            if counter < beef_back_estimate:
                model += y[h] == 1
                counter += 1
            else:
                model += y[h] == 0
        # constraints (1)
        for h in HBF:
            model += y[h] * 1000000 >= xsum(x[h][p][q] for [p, q] in product(PBF, Q))

        for h in HBB:
            model += y[h] * 100000 >= xsum(x[h][p][q] for [p, q] in product(PBB, Q))

        for h in HPC:
            model += y[h] * 100000 >= xsum(x[h][p][q] for [p, q] in product(PPC, Q))

        # # constraints (2)
        for [h, p] in product(HBF, PBF):
            model += xsum(x[h][p][q] for q in Q) - y[h] * part_in_product[p] == 0

        for [h, p] in product(HBB, PBB):
            model += xsum(x[h][p][q] for q in Q) - y[h] * part_in_product[p] == 0

        for [h, p] in product(HPC, PPC):
            model += xsum(x[h][p][q] for q in Q) - y[h] * part_in_product[p] == 0

        # # constraints (3) - products
        for q in Q:
            model += (
                xsum(
                    definition_matrix[q][p] * weight_beef * parameters["usability"].iloc[p] / 2 * x[h][p][q]
                    for [h, p] in product(HBF, PBF)
                )
                + xsum(
                    definition_matrix[q][p] * weight_beef * parameters["usability"].iloc[p] / 2 * x[h][p][q]
                    for [h, p] in product(HBB, PBB)
                )
                + xsum(
                    definition_matrix[q][p] * weight_pork * parameters["usability"].iloc[p] * x[h][p][q]
                    for [h, p] in product(HPC, PPC)
                )
                - needed_kgs[q]
            ) == (z_p[q] - z_n[q])

        if include_limits:
            for q in Q:
                model += z_n[q] <= (1 - volume_limit["coef"].iloc[q]) * needed_kgs[q]

        # # constraints (4)
        for [h, p, q] in product(HBF, PBF, Q):
            model += x[h][p][q] <= 1
            model += x[h][p][q] >= 0
        for [h, p, q] in product(HBB, PBB, Q):
            model += x[h][p][q] <= 1
            model += x[h][p][q] >= 0
        for [h, p, q] in product(HPC, PPC, Q):
            model += x[h][p][q] <= 1
            model += x[h][p][q] >= 0
        for [h, p, q] in product(HBB, PBB, Q):
            model += x[h][p][q] <= definition_matrix[q][p]
        for [h, p, q] in product(HBF, PBF, Q):
            model += x[h][p][q] <= definition_matrix[q][p]
        for [h, p, q] in product(HPC, PPC, Q):
            model += x[h][p][q] <= definition_matrix[q][p]
        for q in Q:
            model += z_n[q] >= 0
            model += z_p[q] >= 0

        # optimizing
        print(model.optimize())

        self.model = model
        self.x = x
        self.y = y
        self.z_n = z_n
        self.z_p = z_p

    def prepare_number_of_parts(self) -> pd.DataFrame:
        sum_pork = 0
        for h in self.HPC:
            if self.y[h].x > 0.99:
                sum_pork += 1

        sum_beef_front = 0
        for h in self.HBF:
            if self.y[h].x > 0.99:
                sum_beef_front += 1

        sum_beef_back = 0
        for h in self.HBB:
            if self.y[h].x > 0.99:
                sum_beef_back += 1
        return pd.DataFrame(
            {"Number of parts": [sum_pork, sum_beef_front, sum_beef_back]},
            index=["Pork", "Beef Front", "Beef Back"],
        )

    def product_availability(self) -> pd.DataFrame:
        diff_product = []
        products = []
        desired = []
        for q in self.Q:
            sum_product = 0
            for [h, p] in product(self.HBF, self.PBF):
                sum_product += (
                    self.input_data.definition.values[q][p]
                    * self.input_data.weight_beef
                    * self.input_data.parameters["usability"].iloc[p]
                    / 2
                    * self.x[h][p][q].x
                )
            for [h, p] in product(self.HBB, self.PBB):
                sum_product += (
                    self.input_data.definition.values[q][p]
                    * self.input_data.weight_beef
                    * self.input_data.parameters["usability"].iloc[p]
                    / 2
                    * self.x[h][p][q].x
                )
            for [h, p] in product(self.HPC, self.PPC):
                sum_product += (
                    self.input_data.definition.values[q][p]
                    * self.input_data.weight_pork
                    * self.input_data.parameters["usability"].iloc[p]
                    * self.x[h][p][q].x
                )
            products.append(sum_product)
            desired.append(float(self.input_data.volumes.iloc[q]))
            diff_product.append(sum_product - float(self.input_data.volumes.iloc[q]))
        return pd.DataFrame(
            {
                "Desired volume": desired,
                "Realized volume": products,
                "Difference from desired volume": diff_product,
            },
            index=self.input_data.volumes.index,
        )

    def pork_leftovers(self) -> pd.DataFrame():
        leftovers = []
        index = []
        for p in self.PPC:
            unused = 0
            for h in self.HPC:
                if self.y[h].x > 0.99:
                    used = 0
                    for q in self.Q:
                        used += self.x[h][p][q].x
                    unused += (
                        self.input_data.weight_pork
                        * float(self.input_data.parameters["usability"].iloc[p])
                        * (1 - used)
                    )
            leftovers.append(unused)
            index.append(self.input_data.parameters.index.values[p])
        return pd.DataFrame({"Leftovers pork": leftovers}, index=index)

    def beef_front_leftovers(self) -> pd.DataFrame():
        leftovers = []
        index = []
        for p in self.PBF:
            unused = 0
            for h in self.HBF:
                if self.y[h].x > 0.99:
                    used = 0
                    for q in self.Q:
                        used += self.x[h][p][q].x
                    unused += (
                        self.input_data.weight_beef
                        * float(self.input_data.parameters["usability"].iloc[p])
                        / 2
                        * (1 - used)
                    )
            leftovers.append(unused)
            index.append(self.input_data.parameters.index.values[p])
        return pd.DataFrame({"Leftovers beef fronts": leftovers}, index=index)

    def beef_back_leftovers(self) -> pd.DataFrame():
        leftovers = []
        index = []
        for p in self.PBB:
            unused = 0
            for h in self.HBB:
                if self.y[h].x > 0.99:
                    used = 0
                    for q in self.Q:
                        used += self.x[h][p][q].x
                    unused += (
                        self.input_data.weight_beef
                        * float(self.input_data.parameters["usability"].iloc[p])
                        / 2
                        * (1 - used)
                    )
            leftovers.append(unused)
            index.append(self.input_data.parameters.index.values[p])
        return pd.DataFrame({"Leftovers beef backs": leftovers}, index=index)

    def get_how_to_cut_pork(self) -> pd.DataFrame():
        animal = []
        part = []
        product = []
        segment = []
        for h in self.HPC:
            if self.y[h].x > 0.99:
                for p in self.PPC:
                    for q in self.Q:
                        if self.x[h][p][q].x > 0:
                            animal.append(h)
                            part.append(self.input_data.parameters.index.values[p])
                            product.append(self.input_data.volumes.index.values[q])
                            segment.append(self.x[h][p][q].x)

        return pd.DataFrame({"id": animal, "part": part, "product": product, "segment": segment})

    def get_how_to_beef_front(self) -> pd.DataFrame():
        animal = []
        part = []
        product = []
        segment = []
        for h in self.HBF:
            if self.y[h].x > 0.99:
                for p in self.PBF:
                    for q in self.Q:
                        if self.x[h][p][q].x > 0:
                            animal.append(h)
                            part.append(self.input_data.parameters.index.values[p])
                            product.append(self.input_data.volumes.index.values[q])
                            segment.append(self.x[h][p][q].x)

        return pd.DataFrame({"id": animal, "part": part, "product": product, "segment": segment})

    def get_how_to_cut_beef_back(self) -> pd.DataFrame():
        animal = []
        part = []
        product = []
        segment = []
        for h in self.HBB:
            if self.y[h].x > 0.99:
                for p in self.PBB:
                    for q in self.Q:
                        if self.x[h][p][q].x > 0:
                            animal.append(h)
                            part.append(self.input_data.parameters.index.values[p])
                            product.append(self.input_data.volumes.index.values[q])
                            segment.append(self.x[h][p][q].x)

        return pd.DataFrame({"id": animal, "part": part, "product": product, "segment": segment})
