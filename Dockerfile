FROM python:3.8
EXPOSE 8501
WORKDIR /app

RUN python -m pip install poetry && poetry --version

COPY pyproject.toml poetry.lock ./
COPY kravicky ./kravicky
COPY app.py README.md ./
COPY data/images ./data/images

ARG POETRY_HTTP_BASIC_AZURE_PASSWORD

RUN poetry install --no-dev


CMD poetry run streamlit run app.py
# --server.headless true \
# --browser.serverAddress="0.0.0.0" \
# --server.enableXsrfProtection false \
# --server.enableCORS false \
# --browser.gatherUsageStats false \
# --server.port 8501
