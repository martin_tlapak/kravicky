# %%
from mip import Model, xsum, minimize, BINARY
from itertools import product
import os
import pandas as pd
import random

# %%
root_path = os.path.dirname(os.getcwd()) + "/data/"
parameters = pd.read_excel(root_path + "parameters.xlsx", index_col="component")
volumes = pd.read_excel(root_path + "volume.xlsx", index_col="product")
definition = pd.read_excel(root_path + "definition.xlsx", index_col="Produkty / Komponenty").fillna(0)
mapping_products = pd.read_excel(root_path + "mapping_products.xlsx", index_col="code ")
mapping_components = pd.read_excel(root_path + "mapping_components.xlsx", index_col="code")
price_products = pd.read_excel(root_path + "price_products.xlsx", index_col="code ")
price_components = pd.read_excel(root_path + "price_components.xlsx", index_col="code")
# %%
number_of_pork_complet = 500
number_of_beef_front = 100
number_of_beef_back = 100
# %%
definition_matrix = definition.values
print(definition_matrix)
# %%
# definition_matrix = [[1,0,0],[1,1,0],[0,0,1],[1,1,1],[0,1,0]]
needed_kgs = volumes.T.values[0]
print(needed_kgs)
# %%
weight_beef = 350
weight_pork = 95

# %%

# %%
H = set(range(number_of_beef_front + number_of_beef_back + number_of_pork_complet))
HBF = set(range(number_of_beef_front))
HBB = set(range(number_of_beef_front, number_of_beef_front + number_of_beef_back))
HPC = set(
    range(
        number_of_beef_front + number_of_beef_back,
        number_of_beef_front + number_of_beef_back + number_of_pork_complet,
    )
)


# Parts
P = set(range(len(parameters)))
PBF = [item for item in P if parameters["section"].iloc[item] == "P"]
PBB = [item for item in P if parameters["section"].iloc[item] == "Z"]
PPC = [item for item in P if parameters["section"].iloc[item] == "C"]

# Products
Q = set(range(len(needed_kgs)))
# %%
model = Model()

# continuous variable x_{hpq}
x = [[[model.add_var() for q in Q] for p in P] for h in H]

# binary variable y_{h}
y = [model.add_var(var_type=BINARY) for h in H]

# continous variable z_{q}
z_p = [model.add_var() for q in Q]
z_n = [model.add_var() for q in Q]
# objective function: minimize number of killed cows
# model.objective = minimize(
#     xsum(y[h] for h in H)
#     + xsum(x[h][p][q] for [h, p, q] in product(HBF, PBF, Q))
#     + xsum(x[h][p][q] for [h, p, q] in product(HBB, PBB, Q))
#     + xsum(x[h][p][q] for [h, p, q] in product(HPC, PPC, Q))
# )

model.objective = minimize(
    xsum(
        price_components["price"].iloc[p]
        * (
            y[h] * weight_beef * parameters["usability"].iloc[p]
            - (xsum(x[h][p][q] * weight_beef * parameters["usability"].iloc[p] for q in Q))
        )
        for [h, p] in product(HBB, PBB)
    )
    + xsum(
        price_components["price"].iloc[p]
        * (
            y[h] * weight_beef * parameters["usability"].iloc[p]
            - (xsum(x[h][p][q] * weight_beef * parameters["usability"].iloc[p] for q in Q))
        )
        for [h, p] in product(HBF, PBF)
    )
    + xsum(
        price_components["price"].iloc[p]
        * (
            y[h] * weight_pork * parameters["usability"].iloc[p]
            - (xsum(x[h][p][q] * weight_pork * parameters["usability"].iloc[p] for q in Q))
        )
        for [h, p] in product(HPC, PPC)
    )
    + xsum(-price_products["price"].iloc[q] * (needed_kgs[q] - z_n[q]) for q in Q)
    + xsum(price_products["price"].iloc[q] * z_p[q] for q in Q)
)
# constraints (1)
for h in HBF:
    model += y[h] * len(PBF) >= xsum(x[h][p][q] for [p, q] in product(PBF, Q))

for h in HBB:
    model += y[h] * len(PBB) >= xsum(x[h][p][q] for [p, q] in product(PBB, Q))

for h in HPC:
    model += y[h] * len(PPC) >= xsum(x[h][p][q] for [p, q] in product(PPC, Q))


# # constraints (2)
for [h, p] in product(HBF, PBF):
    model += xsum(x[h][p][q] for q in Q) <= 1

for [h, p] in product(HBB, PBB):
    model += xsum(x[h][p][q] for q in Q) <= 1

for [h, p] in product(HPC, PPC):
    model += xsum(x[h][p][q] for q in Q) <= 1

# # constraints (3) - products
for q in Q:
    model += (
        xsum(
            definition_matrix[q][p] * weight_beef * parameters["usability"].iloc[p] * x[h][p][q]
            for [h, p] in product(HBF, PBF)
        )
        + xsum(
            definition_matrix[q][p] * weight_beef * parameters["usability"].iloc[p] * x[h][p][q]
            for [h, p] in product(HBB, PBB)
        )
        + xsum(
            definition_matrix[q][p] * weight_pork * parameters["usability"].iloc[p] * x[h][p][q]
            for [h, p] in product(HPC, PPC)
        )
        - needed_kgs[q]
    ) == (z_p[q] - z_n[q])

for q in Q:
    model += z_n[q] <= 0.2 * needed_kgs[q]

# # constraints (4)
for [h, p, q] in product(HBF, PBF, Q):
    model += x[h][p][q] <= 1
    model += x[h][p][q] >= 0
for [h, p, q] in product(HBB, PBB, Q):
    model += x[h][p][q] <= 1
    model += x[h][p][q] >= 0
for [h, p, q] in product(HPC, PPC, Q):
    model += x[h][p][q] <= 1
    model += x[h][p][q] >= 0
for [h, p, q] in product(HBB, PBB, Q):
    model += x[h][p][q] <= definition_matrix[q][p]
for [h, p, q] in product(HBF, PBF, Q):
    model += x[h][p][q] <= definition_matrix[q][p]
for [h, p, q] in product(HPC, PPC, Q):
    model += x[h][p][q] <= definition_matrix[q][p]
for q in Q:
    model += z_n[q] >= 0
    model += z_p[q] >= 0

# optimizing
model.optimize()

# %%
sum_pork = 0
for h in HPC:
    if y[h].x > 0.99:
        sum_pork += 1
print(f"Kill pork: {sum_pork}")

sum_beef_front = 0
for h in HBF:
    if y[h].x > 0.99:
        sum_beef_front += 1
print(f"Kill beef front: {sum_beef_front}")


sum_beef_back = 0
for h in HBB:
    if y[h].x > 0.99:
        sum_beef_back += 1
print(f"Kill beef back: {sum_beef_back}")

# %%
for q in Q:
    sum_product = 0
    for [h, p] in product(HBF, PBF):
        sum_product += definition_matrix[q][p] * weight_beef * parameters["usability"].iloc[p] * x[h][p][q].x
    for [h, p] in product(HBB, PBB):
        sum_product += definition_matrix[q][p] * weight_beef * parameters["usability"].iloc[p] * x[h][p][q].x
    for [h, p] in product(HPC, PPC):
        sum_product += definition_matrix[q][p] * weight_pork * parameters["usability"].iloc[p] * x[h][p][q].x

    diff_product = sum_product - needed_kgs[q]
    print(f'Product {mapping_products["name"].iloc[q]}: {diff_product}')
# %%

for p in PPC:
    unused = 0
    for h in HPC:
        if y[h].x > 0.99:
            used = 0
            for q in Q:
                used += x[h][p][q].x
            unused += weight_pork * parameters["usability"].iloc[p] * (1 - used)
    print(f"Unused of part {mapping_components['name'].iloc[p]} is {unused} kg.")

# %%
for p in PBF:
    unused = 0
    for h in HBF:
        if y[h].x > 0.99:
            used = 0
            for q in Q:
                used += x[h][p][q].x
            unused += weight_pork * parameters["usability"].iloc[p] * (1 - used)
    print(f"Unused of part {mapping_components['name'].iloc[p]} is {unused} kg.")
# %%
for p in PBB:
    unused = 0
    for h in HBB:
        if y[h].x > 0.99:
            used = 0
            for q in Q:
                used += x[h][p][q].x
            unused += weight_pork * parameters["usability"].iloc[p] * (1 - used)
    print(f"Unused of part {mapping_components['name'].iloc[p]} is {unused} kg.")
# %%
